A field formatter for text fields that automatically renders the embedded version of a video when you provide a URL to either a Youtube or Vimeo video.

You must have 'Enable Colorbox load' and 'Enable Colorbox inline' enabled in the colorbox module.